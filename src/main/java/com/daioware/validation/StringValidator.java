package com.daioware.validation;

import java.util.regex.Pattern;

public class StringValidator {
	public static final Pattern mailPattern=
			Pattern.compile("^[_a-z0-9-]+(.[_a-z0-9-]+)*@[_a-z0-9-]+(.[_a-z0-9-]+)+$");
	
	public static final Pattern usernamePattern=
			Pattern.compile("[a-zA-Z_]+[{a-zA-Z}|{0-9}|_]*$");
	
	public static final Pattern pwdPattern = Pattern
			.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%-?!%]).{6,100})");
	
	public static boolean validateSimple(String string,int minLength,int maxLength) {
		int length;
		return string!=null && (length=string.length())>=minLength && length<=maxLength;
	}
	public static boolean validateSimple(String string,int maxLength) {
		return validateSimple(string,1,maxLength);
	}
	public static boolean isValidEmail(String possibleMail) {
		return possibleMail!=null && mailPattern.matcher(possibleMail).matches();
	}
	public static boolean isValidUsername(String username,int maxLength) {
		return username!=null?usernamePattern.matcher(username).matches() && username.length()<=maxLength:false;
	}
	public static boolean isValidUsername(String username) {
		return username!=null?usernamePattern.matcher(username).matches():false;
	}
	
	public static boolean isValidPassword(String password) {
		return password!=null?pwdPattern.matcher(password).matches():false;
	}
	public static boolean isValidPassword(String password,int minLength,int maxLength) {
		int length;
		return password!=null?(length=password.length())>=minLength && length<=maxLength && 
				pwdPattern.matcher(password).matches():false;
	}
}
