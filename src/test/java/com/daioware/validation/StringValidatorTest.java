package com.daioware.validation;

import static com.daioware.validation.StringValidator.*;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class StringValidatorTest {
	
	@Test
	public void test(){
		assertTrue(isValidPassword("sTs;9-"));
		assertTrue(isValidEmail("diego@123diego.4363.com.902p"));
	}
}
